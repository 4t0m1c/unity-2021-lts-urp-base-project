# Unity 2021 LTS URP Base Project

## Getting started

Download/clone this repo and move it over to your own repo. Rename the Unity project before opening.

## Features

- Unity 2021.3.4f1
- URP Core Template
- Project Settings:
    - .Net Framework
    - Incremental GC
- Packages:
    - Cinemachine
    - Input System
    - 2D Sprite
- Assets:
    - DoTween
    - Farland Skies
    - Easy Buttons
    - UI Shapes & Icons
    - Common Utility Scripts
