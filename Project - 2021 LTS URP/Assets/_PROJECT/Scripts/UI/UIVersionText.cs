﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIVersionText : MonoBehaviour {

    void Start () {
        GetComponent<TMPro.TMP_Text> ().text = "v. " + Application.version;
    }

}